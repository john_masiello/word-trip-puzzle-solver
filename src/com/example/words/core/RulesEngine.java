package com.example.words.core;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Collector.Characteristics;

import com.example.words.config.Configuration;

public class RulesEngine {
	
	private Configuration config;

	private final Predicate<String> selectAll = str -> true;
	private final Predicate<String> selectNone = str -> false;
	
	public RulesEngine() {
		config = new Configuration();
	}

	public RulesEngine(Configuration configuration) {
		this.config = configuration;
	}

	public static String beginningOfWord(String pattern) {
		return "/" + pattern;
	}
	
	public static String endingOfWord(String pattern) {
		return pattern + "/";
	}
	
	public static String wildcard() {
		return "?";
	}
	
	public Set<String> filterByAnyOf(Set<String> possibleWords, String ...pattern) {
		Predicate<String> filterPattern = null;
		if (pattern.length == 0) {
			filterPattern = selectAll;

		} else {
			filterPattern = config.isRegex() ? asPredicate(pattern[0], selectAll) :
			asPredicate(toRegexInclude(pattern), selectAll)
				.and(asPredicate(toRegexExclude(pattern), selectNone).negate())
				.and(rangeAsPredicate(pattern));
		}
		return possibleWords.stream()
			.filter(filterPattern)
			.collect(Collectors.toCollection(collectionFactory()));
	}
	
	Supplier<Set<String>> collectionFactory() {
		return () -> config.isSorted() ? new TreeSet<String>() :
			new HashSet<String>();
	}
	
	Predicate<String> asPredicate(String regex, Predicate<String> fallback) {
		return regex.isEmpty() ? fallback :
			Pattern.compile(regex).asPredicate();
	}
	
	String toRegexInclude(String ...pattern) {
		return Arrays.asList(pattern).stream()
			.filter(isNegative().negate().and(isRange().negate()))
			.collect(Collectors.mapping(this::toRegexGroup, 
					Collectors.joining("|")));
	}
	
	String toRegexExclude(String ...pattern) {
		return Arrays.asList(pattern).stream()
			.filter(isNegative())
			.map(this::stripNegative)
			.collect(Collectors.mapping(this::toRegexGroup, 
					Collectors.joining("|")));
	}

	Predicate<String> rangeAsPredicate(String ...pattern) {
		Predicate<String> acceptRange = selectAll;
		List<String> bounds = Arrays.asList(pattern).stream()
			.filter(isRange())
			.collect(Collector.of(
				() -> Arrays.<String>asList(null, null),
				(range, str) -> parseRangePattern(range, str),
				(range1, range2) -> range1,
				Characteristics.UNORDERED
			));
		if (bounds.get(0) != null) {
			String min = bounds.get(0);
			acceptRange = acceptRange.and(word -> min.compareTo(word) <= 0);
		}
		if (bounds.get(1) != null) {
			String max = bounds.get(1);
			acceptRange = acceptRange.and(word -> max.compareTo(word) >= 0);
		}
		return acceptRange;
	}

	private void parseRangePattern(List<String> bounds, String pattern) {
		try {
			String lowerbound = pattern.substring(1, pattern.indexOf(','));
			String upperbound = pattern.substring(pattern.indexOf(',') + 1, pattern.length() - 1);

			if (!lowerbound.isEmpty() && (bounds.get(0) == null
				|| bounds.get(0).compareTo(lowerbound) > 0))
				bounds.set(0, lowerbound);
			if (!upperbound.isEmpty() && (bounds.get(1) == null
				|| bounds.get(1).compareTo(upperbound) < 0))
				bounds.set(1, upperbound);

		} catch (Throwable ignore) {

		}
	}
	
	private Predicate<String> isNegative() {
		return pattern -> {
			switch (pattern.charAt(0)) {
			case '~':
			case '!':
				return true;
			default:
				return false;
			}
		};
	}

	private Predicate<String> isRange() {
		return pattern -> pattern.charAt(0) == '[';
	}
	
	private String stripNegative(String pattern) {
		return pattern.substring(1);
	}
	
	private String toRegexGroup(String pattern) {		
		return new StringBuilder("(")
			.append(pattern.replaceFirst("^/", "^") // starting '/'
					// ^[char sequence]
										
					.replaceAll("/$", "\\$") // terminating '/'
					// [char sequence]$
					
					.replace("/", "") // remaining '/' characters
					.replace("?", "\\w")) // wildcard '?'
					// \w any letter
			.append(")").toString();
	}
}