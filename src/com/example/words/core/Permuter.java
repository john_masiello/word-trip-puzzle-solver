package com.example.words.core;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.example.words.config.Configuration;

public class Permuter {
	private Configuration config;

	public Permuter() {
		config = new Configuration();
	}

	public Permuter(Configuration config) {
		this.config = config;
	}

	public Set<String> permute(String letters) {
		Set<String> permutations = new TreeSet<>();
		int length = letters.length();
		
		int[] choices = new int[length];
		String[] subsetLetters = new String[length];
		
		int step = length - 1;
		int index;
		subsetLetters[step] = letters;
		String _letters;
		
		String permutation = "";
		
		while (step < length) {
			if (choices[step] < step + 1) {
				index = choices[step];
				_letters = subsetLetters[step];
				/* Construct the permutation by concatenating the partial permutation
				 * by the previous steps with our choice of letter at {index} position
				 * for this step
				 */
				permutation = permutation.substring(0, length - step - 1)
						+ String.valueOf(_letters.charAt(index));
				
				if (step == 0) {
					/* 
					 * Permutations is completed, at length number of steps
					 */
					permutations.add(permutation);
					step++;
					
				} else {
					/* 
					 * Keep decrementing number of steps, until 
					 * permutation is built for {length} number of steps
					 */ 
					choices[step]++;
					step--;
					// Initialize choice value for next step
					choices[step] = 0;
					// Initialize subset of remaining letters for next step,
					// removing our choice of letter for this given step
					subsetLetters[step] = removeChar(_letters, index);
				}
				
			} else {
				/* 
				 * All choices have been exhausted
  				 * Proceed with our iteration by reverting a step,
  				 * or complete when there are no more steps to revert
				 */
				step++;
			}
		}
		return permutations;
	}
	
	private String removeChar(String str, int at) {
		return str.substring(0, at) + str.substring(at + 1);
	}
	
	public Set<String> permute(String letters, int wordLength) {
		boolean parallel = true;
		return StreamSupport.stream(permute(letters).spliterator(), parallel)
				.map(word -> word.substring(0, wordLength))
				.collect(Collectors.toCollection(collectionFactory()));
	}
	
	Supplier<Set<String>> collectionFactory() {
		return () -> config.isSorted() ? new TreeSet<String>() :
			new HashSet<String>();
	}
}