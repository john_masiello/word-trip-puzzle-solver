package com.example.words.config;

public class Configuration {
    private static final int NUM_COLUMNS;

    private static final boolean IS_SORTED;

    private static final boolean IS_REGEX;

    private static final Print PRINT;

    static
    {
        NUM_COLUMNS = Integer.parseInt(System.getProperty("columns", "10"));

        IS_SORTED = Boolean.parseBoolean(System.getProperty("sorted", "true"));

        IS_REGEX = Boolean.parseBoolean(System.getProperty("regex", "false"));

        PRINT = Print.valueOf(System.getProperty("print", Print.NORMAL.name()).toUpperCase());
    }

    /**
     * @return the numColumns
     */
    public int getNumColumns() {
        return NUM_COLUMNS;
    }

    /**
     * @return the isSorted
     */
    public boolean isSorted() {
        return IS_SORTED;
    }

    /**
     * @return the isRegex
     */
    public boolean isRegex() {
        return IS_REGEX;
    }

    /**
     * 
     * @return Print type
     */
    public Print getPrintType() {
        return PRINT;
    }
}