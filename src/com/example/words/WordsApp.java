package com.example.words;

import java.util.Arrays;
import java.util.Set;

import com.example.words.core.Permuter;
import com.example.words.core.RulesEngine;
import com.example.words.util.WordsPrinter;

public class WordsApp {

	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("{}");
			return;
		}
		String rootWord = args[0];
		if (rootWord.toLowerCase().contains("help")) {
			System.out.println("\n"
					+ "Syntax:\n\n\tWordsApp "
					
					+ "{Word consisting of letters} "
					+ "{total number of letters} "
					+ "{optional: varargs patterns}"
					
					+ "\n\n\t-patterns: an 'OR' query for any of "
					
					+ "\n\n\t\t-containing \t\t[string literal]"
					+ "\n\t\t-starts with\t\t '/' followed by [string literal]"
					+ "\n\t\t-ends with\t\t [string literal] followed by a '/'"
					+ "\n\t\t-does not contain \t '~' or '!' followed by [pattern]" 
					+ "\n\n"
					+ "\tJVM args:"
					+ "\n\n\t\t-columns\t\t number of columns to print"
					+ "\n\t\t-regex\t\t\t patterns are regex\t\t 'true'|'false'"
					+ "\n\t\t-sorted\t\t\t results are sorted\t\t 'true'|'false'"
					+ "\n");
			return;
		}
		String[] patterns = Arrays.copyOfRange(args, 2, args.length);
		Set<String> permutations = new Permuter().permute(rootWord, Integer.valueOf(args[1]));
		Set<String> filteredWords = new RulesEngine()
				.filterByAnyOf(permutations, patterns);
		WordsPrinter.print(filteredWords, System.out);
	}
}
