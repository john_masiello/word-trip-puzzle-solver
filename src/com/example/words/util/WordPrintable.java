package com.example.words.util;

import java.io.PrintStream;
import java.util.Set;

public interface WordPrintable {
    public void print(Set<String> words, PrintStream ps);
}