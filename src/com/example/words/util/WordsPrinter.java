package com.example.words.util;

import java.io.PrintStream;
import java.util.Set;

import com.example.words.config.Configuration;

public class WordsPrinter {
    private static Configuration config = new Configuration();
    
    public static void print(Set<String> words, PrintStream ps) {
        getWordPrintable().print(words, ps);
    }

    static WordPrintable getWordPrintable() {
        switch (config.getPrintType()) {
            case BULK:
                return new WordsPrinterBlockImpl();
            default:
                return new WordsPrinterByLeadingLettersImpl();
        }
    }
}