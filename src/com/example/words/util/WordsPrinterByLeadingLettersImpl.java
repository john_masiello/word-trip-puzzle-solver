package com.example.words.util;

import java.io.PrintStream;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.example.words.config.Configuration;

public class WordsPrinterByLeadingLettersImpl implements WordPrintable {
	private Configuration config = new Configuration();
	
	final static String WORD_SEPARATOR = "  ";
	final static String LINE_SEPARATOR = System.lineSeparator();
	final static String BEGIN_BLOCK = "[";
	final static String END_BLOCK = "]";
	final static String INDENT = " ";
	
	public void print(Set<String> words, PrintStream ps) {
		words.stream()
		.collect(Collectors.groupingBy(word -> word.substring(0, 2),
			TreeMap::new,
			Collectors.toList()))
			.values().stream()
				.forEach(group -> printGroup(group, ps));
		ps.println("END OF WORDS");
		ps.println();
	}

	private void printGroup(List<String> words, PrintStream ps) {
		StringBuilder s = new StringBuilder(BEGIN_BLOCK)
			.append(words.get(0));
		IntStream.range(1, words.size())
			.forEach(i -> {
				if (i % columns() == 0) {
					s.append(LINE_SEPARATOR)
						.append(INDENT);
				} else {
					s.append(WORD_SEPARATOR);
				}
				s.append(words.get(i));
			});
		s.append(END_BLOCK)
			.append(LINE_SEPARATOR);
		ps.println(s);
	}
	
	int columns() {
		return config.getNumColumns();
	}
	
	boolean isSorted() {
		return config.isSorted();
	}
}