package com.example.words.util;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.example.words.config.Configuration;

public class WordsPrinterBlockImpl implements WordPrintable {
	private Configuration config = new Configuration();
	
	final static String WORD_SEPARATOR = "\t";
	final static String LINE_SEPARATOR = System.lineSeparator();
	
	public void print(Set<String> words, PrintStream ps) {
		try(OutputStreamWriter os = new OutputStreamWriter(new BufferedOutputStream(ps, 512))) {
			append("[", os);
			final boolean parallel = !isSorted();
			List<List<String>> groupedWords =
			StreamSupport.stream(words.spliterator(), parallel)
				.collect(Collector.<String, List<List<String>>>of(ArrayList<List<String>>::new, 
						(container, word) -> {
							Optional<List<String>> maybeGroup = findGroup(container);
							maybeGroup.ifPresent(foundGroup -> {
								foundGroup.add(word);
							});
							if (!maybeGroup.isPresent())
								container.add(new ArrayList<>(Arrays.asList(word)));
						}, 
						(c1, c2) -> {
							// Merge <= 1 group from c2 with <= 1 group from c1
							Optional<List<String>> maybeGroup =
									c2.stream().filter(this::isIncomplete).findAny();
							maybeGroup.ifPresent(foundGroup -> {
								Optional<List<String>> maybeFormerGroup = findGroup(c1);
								maybeFormerGroup.ifPresent(foundFormerGroup -> {
									int index = columns() - foundFormerGroup.size() < foundGroup.size() ?
											columns() - foundFormerGroup.size() :
												foundGroup.size();
									foundFormerGroup.addAll(foundGroup.subList(0, index));
									List<String> remainingItemsInGroup = foundGroup.subList(index, 
											foundGroup.size());
									if (!remainingItemsInGroup.isEmpty())
										c1.add(remainingItemsInGroup);
								});
								if (!maybeFormerGroup.isPresent())
									c1.add(foundGroup);
							});
							// Combine c1 and c2 by adding the remaining elements of c2 to c1
							List<List<String>> combined = c2.stream().filter(this::isComplete)
								.collect(Collectors.collectingAndThen(Collectors.toList(), 
										list -> {
											c1.addAll(list);
											return c1;
										}));
							return combined;
						}, 
						Collector.Characteristics.UNORDERED));
			
			// Parallel stream lines to the print stream
			Stream<List<String>> wordGroup;
			wordGroup = StreamSupport.stream(groupedWords.spliterator(), parallel)
					.filter(this::isComplete);
			printWordGroup(wordGroup, os);
			// Print the incomplete word group last
			wordGroup = StreamSupport.stream(groupedWords.spliterator(), parallel)
					.filter(this::isIncomplete);
			printWordGroup(wordGroup, os);
			
			append(LINE_SEPARATOR + "]", os);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void printWordGroup(Stream<List<String>> wordGroup, OutputStreamWriter os) {
		wordGroup.forEach(groupWords -> {
			String line =
					groupWords.stream()
						.reduce(LINE_SEPARATOR,
								(_line, word) -> _line + word + WORD_SEPARATOR);
				 append(line, os);
			});
	}
	
	private Optional<List<String>> findGroup(List<List<String>> container) {
		return container.stream()
			.filter(this::isIncomplete)
			.findAny();
	}
	
	private boolean isComplete(List<String> group) {
		return group.size() == columns();
	}
	
	private boolean isIncomplete(List<String> group) {
		return group.size() < columns();
	}
	
	private void append(String str, OutputStreamWriter os) {
		try {
			os.append(str);
			
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	int columns() {
		return config.getNumColumns();
	}
	
	boolean isSorted() {
		return config.isSorted();
	}
}