package com.example.words;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.example.words.config.Configuration;

public class BaseTestCase {

	@Spy
	private Configuration configuration;
	
	@Before
	public final void _setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	public Configuration getConfiguration() {
		return configuration;
	}
}
