package com.example.words.core;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.example.words.BaseTestCase;

public class PermuterTest extends BaseTestCase {
	
	@InjectMocks
	private Permuter permuter;
	
	@Test
	public void testConstucts() {
		assertNotNull(new Permuter());
	}
	
	@Test
	public void testPermutationOfTwoSorted() {
		doReturn(true).when(getConfiguration()).isSorted();
		MatcherAssert.assertThat(permuter.permute("ab"), Matchers.contains(
				"ab", "ba"));
	}
	
	@Test
	public void testPermutationOfThreeUnsorted() {
		doReturn(false).when(getConfiguration()).isSorted();
		MatcherAssert.assertThat(permuter.permute("abc"), Matchers.containsInAnyOrder(
				"abc", "acb", "bac", "bca", "cab", "cba"));
	}
	
	@Test
	public void testPermutationOfThreeOfLengthTwoSorted() {
		doReturn(true).when(getConfiguration()).isSorted();
		MatcherAssert.assertThat(permuter.permute("abc", 2), Matchers.contains(
				"ab", "ac", "ba", "bc", "ca", "cb"));
	}
	
	@Test
	public void testPermutationOfThreeOfLengthTwoUnSorted() {
		doReturn(false).when(getConfiguration()).isSorted();
		MatcherAssert.assertThat(permuter.permute("abc", 2), Matchers.containsInAnyOrder(
				"ab", "ac", "ba", "bc", "ca", "cb"));
	}
}
