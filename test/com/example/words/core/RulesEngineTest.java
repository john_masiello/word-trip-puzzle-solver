package com.example.words.core;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hamcrest.MatcherAssert;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.example.words.BaseTestCase;

public class RulesEngineTest extends BaseTestCase {
	
	@InjectMocks
	private RulesEngine rulesEngine;
	
	@Before
	public void setUp() {
		lenient().doReturn(false).when(getConfiguration()).isRegex();
		lenient().doReturn(false).when(getConfiguration()).isSorted();
	}
	
	@Test
	public void testConstructs() {
		assertNotNull(new RulesEngine());
	}
	
	@Test
	public void testBeginningOfWordsPrependsForwardSlash() {
		Assert.assertEquals("/a", RulesEngine.beginningOfWord("a"));
	}
	
	@Test
	public void testEndingOfWordsAppendsForwardSlash() {
		Assert.assertEquals("a/", RulesEngine.endingOfWord("a"));
	}
	
	@Test
	public void testWildCard() {
		Assert.assertEquals("?", RulesEngine.wildcard());
	}
	
	@Test
	@Ignore
	public void testFilterByAnyOfNoPatternsSpecifiesReturnsEmptySet() {
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc")));
		MatcherAssert.assertThat(set, containsInAnyOrder());
	}
	
	@Test
	public void testFilterByAnyOfStartsWithAReturnsASingleEntrySet() {
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc")), 
				RulesEngine.beginningOfWord("a"));
		MatcherAssert.assertThat(set, containsInAnyOrder("abc"));
	}
	
	@Test
	public void testFilterByAnyOfEndWithCReturnsASingleEntrySet() {
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc")), 
				RulesEngine.endingOfWord("c"));
		MatcherAssert.assertThat(set, containsInAnyOrder("abc"));
	}
	
	@Test
	public void testFilterByAnyOfContainsABReturnsASingleEntrySet() {
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc")), 
				"ab");
		MatcherAssert.assertThat(set, containsInAnyOrder("abc"));
	}
	
	@Test
	public void testWildCardMatchingReturnsASingleEntrySet() {
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc")), 
				"?b");
		MatcherAssert.assertThat(set, containsInAnyOrder("abc"));
	}
	
	@Test
	public void testWildCardWithBeginningAnchorReturnsASingleEntrySet() {
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc")), 
				"/?b");
		MatcherAssert.assertThat(set, containsInAnyOrder("abc"));
	}
	
	@Test
	public void testWildCardAndAnchorDoNotMatchForWrongPattern() {
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc")), 
				"/?c");
		MatcherAssert.assertThat(set, not(containsInAnyOrder("abc")));
	}
	
	@Test
	public void testSortedResultWithNonRegexPattern() {
		doReturn(true).when(getConfiguration()).isSorted();
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc", "bca")), 
				"/?");
		MatcherAssert.assertThat(set, contains("abc", "bca"));
	}
	
	@Test
	public void testSortedResultWithRegexPattern() {
		doReturn(true).when(getConfiguration()).isRegex();
		doReturn(true).when(getConfiguration()).isSorted();
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc", "bca")), 
				"(bc)");
		MatcherAssert.assertThat(set, contains("abc", "bca"));
	}
	
	@Test
	public void testWithRegexPattern() {
		doReturn(true).when(getConfiguration()).isRegex();
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc", "bca")), 
				"^((?!ca).)+$");
		MatcherAssert.assertThat(set, contains("abc"));
	}
	
	@Test
	public void testNegatePattern() {
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc", "bca")), 
				"~ca", "???");
		MatcherAssert.assertThat(set, contains("abc"));
	}
	
	@Test
	@Ignore
	public void testWildcards() {
		String str = "abcd";
		Pattern pattern = Pattern.compile("^a\\w{1}c");
		Matcher matcher = pattern.matcher(str);
		Assert.assertTrue(matcher.find());
		Assert.assertTrue(Pattern.matches("a\\w{2}d$", str));
		Assert.assertTrue(Pattern.matches("(a\\w{2}d$)|(^a)", str));
		Assert.assertTrue(Pattern.matches("(^a)|(a\\w{2}d$)", str));
	}

	@Test
	public void testRange() {
		Set<String> set = rulesEngine.filterByAnyOf(
				new TreeSet<String>(Arrays.asList("abc", "bca")), 
				"[abc,abc]");
		MatcherAssert.assertThat(set, contains("abc"));
	}
}
